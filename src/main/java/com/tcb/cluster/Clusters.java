package com.tcb.cluster;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.tcb.matrix.LabeledMatrix;
import com.tcb.matrix.Matrix;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.tree.node.Node;
import com.tcb.tree.tree.LeafNodeTreeSearcher;
import com.tcb.tree.tree.Tree;

public class Clusters {
	
	public static double[] getPairDistances(
			Node cluster1, Node cluster2,
			Tree tree,
			LabeledMatrix<Node> nodeDistances){
		List<Node> data1 = getLeafNodes(cluster1,tree);
		List<Node> data2 = getLeafNodes(cluster2,tree);
		
		final int size1 = data1.size();
		final int size2 = data2.size();
		double[] pairDistances = new double[size1 * size2];
			
		for(int i=0;i<size1;i++){
			for(int j=0;j<size2;j++){
				Node a = data1.get(i);
				Node b = data2.get(j);
				int idx = size2*i + j;
				pairDistances[idx] = nodeDistances.get(a, b);
			}
		}			
		
		return pairDistances;
	}
	
	private static List<Node> getLeafNodes(Node cluster, Tree tree){
		return LeafNodeTreeSearcher.getLeafNodes(cluster, tree);
	}
}
