package com.tcb.cluster.limit;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.tcb.cluster.Cluster;
import com.tcb.cluster.ClusterImpl;
import com.tcb.cluster.ClusterTree;
import com.tcb.cluster.ClusterStep;
import com.tcb.cluster.log.LogBuilder;
import com.tcb.matrix.TriangularMatrix;

public class TargetNumberLimit implements ClusterLimit {

	private final Integer number;

	public TargetNumberLimit(int number){
		this.number = number;
		verifyInput();
	}
	
	private void verifyInput(){
		if(number < 1) throw new IllegalArgumentException("Target number must be >= 1");
	}
	
	@Override
	public Boolean finished(ClusterStep clusterStep) {
		if(clusterStep.clusterCount <= 1) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String reportParameters() {
		LogBuilder log = new LogBuilder();
		log.write(String.format("Limit: %s (%d)", "Target number", number));
		return log.get();
	}

	@Override
	public List<Cluster> getClusters(ClusterTree result) {
		return result.getClusters(number);
	}
	
}
