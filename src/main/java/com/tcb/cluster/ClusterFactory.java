package com.tcb.cluster;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.tcb.tree.identifiable.SuidFactory;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.tree.node.Node;
import com.tcb.tree.node.NodeFactory;
import com.tcb.tree.node.NodeImpl;
import com.tcb.tree.tree.Tree;
import com.tcb.tree.tree.TreeImpl;

public class ClusterFactory {
	private final Integer startClusterCount;

	public ClusterFactory(Integer startClusterCount){
		this.startClusterCount = startClusterCount;
	}
	
	public Tree createStartClusters(){
		Node root = NodeFactory.create();
		Tree tree = TreeImpl.create(root);
		
		for(int i=0;i<startClusterCount;i++){
			Node node = NodeFactory.create();
			tree.addNode(node, root);
		}
		return tree;
	}	
}
