package com.tcb.cluster.linkage;

import java.util.Map;

import com.tcb.matrix.TriangularMatrix;

public enum LinkageStrategy {
	SINGLE, COMPLETE, AVERAGE;
	
	public Linkage getLinkage(){
		switch(this){
		case SINGLE: return new SingleLinkage();
		case COMPLETE: return new CompleteLinkage();
		case AVERAGE: return new AverageLinkage();
		default: throw new IllegalArgumentException("Unknown LinkageStrategy");
		}
	}
	
	public String toString(){
		switch(this){
		case SINGLE: return "Single";
		case COMPLETE: return "Complete";
		case AVERAGE: return "Average";
		default: throw new IllegalArgumentException("Unknown LinkageStrategy");
		}
	}
}
