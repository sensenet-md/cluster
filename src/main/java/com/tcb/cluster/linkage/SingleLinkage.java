package com.tcb.cluster.linkage;

import java.util.Map;

import com.tcb.cluster.Cluster;
import com.tcb.cluster.Clusters;
import com.tcb.cluster.log.LogBuilder;
import com.tcb.matrix.LabeledMatrix;
import com.tcb.matrix.Matrix;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.tree.node.Node;
import com.tcb.tree.tree.Tree;
import com.tcb.common.util.ArrayUtil;

public class SingleLinkage implements Linkage {

	@Override
	public double getDistance(Node a, Node b, Tree tree, LabeledMatrix<Node> nodeDistances) {
		double[] pairDistances = Clusters.getPairDistances(a,b, tree, nodeDistances);
		return ArrayUtil.getMin(pairDistances);
	}
	
	@Override
	public String reportParameters() {
		LogBuilder log = new LogBuilder();
		log.write(String.format("Linkage: %s", LinkageStrategy.SINGLE.toString()));
		return log.get();
	}
	
	
	
}
