package com.tcb.cluster;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;

import com.tcb.cluster.ClusterImpl;
import com.tcb.cluster.agglomerative.AgglomerativeClusterer;
import com.tcb.cluster.limit.ClusterLimit;
import com.tcb.cluster.limit.TargetNumberLimit;
import com.tcb.cluster.linkage.CompleteLinkage;
import com.tcb.matrix.LabeledMatrix;
import com.tcb.matrix.LabeledSquareMatrixImpl;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.common.util.Rounder;
import com.tcb.common.util.SafeMap;

public class ClusterTreeTest {
	
	private Map<String,List<Double>> irisData;
			
	private List<List<Integer>> refCompleteLinkageSet1 =
			Arrays.asList(
					Arrays.asList(11, 14, 16, 18, 20, 23, 24),
					Arrays.asList(1, 2, 3, 6, 8, 9),
					Arrays.asList(0, 4, 7),
					Arrays.asList(12, 22, 29),
					Arrays.asList(13, 15, 21),
					Arrays.asList(17, 19, 26),
					Arrays.asList(25, 27),
					Arrays.asList(5),
					Arrays.asList(10),
					Arrays.asList(28)
					);
	
	private List<String> labels = IntStream.range(0,30)
			.boxed()
			.map(i -> i.toString())
			.collect(Collectors.toList());

	private ClusterTree clusterTree;
	
	@Before
	public void setUp() throws Exception {
		this.irisData = new ClusterTestData().readShortData();
		TriangularMatrix distances = 
				ClusterTestData.getDistances(
						irisData.get("sepalLengths"), irisData.get("sepalWidths"));
				LabeledMatrix<String> matrix = LabeledSquareMatrixImpl.create(labels, distances);
		TreeClusterer clusterer = new AgglomerativeClusterer(
				matrix,
				new CompleteLinkage());
		this.clusterTree = clusterer.cluster();
	}

	@Test
	public void testGetCentroids() {
		List<Cluster> clusters = clusterTree.getClusters(10);
		
		List<Integer> centroids = clusters.stream()
				.map(c -> c.getCentroid())
				.map(s -> Integer.valueOf(s))
				.collect(Collectors.toList());
		
		assertEquals(Arrays.asList(24,2,0,12,21,26,25,5,10,28), centroids);
	}
	
	@Test
	public void testGetCentroidSquaredErrors() {
		// TODO Check reference values!
		
		List<Cluster> clusters = clusterTree.getClusters(10);
		List<Double> ref = Arrays.asList(0.42,0.38,0.04,0.39,0.27,0.14,0.1,0.0,0.0,0.0);
				
		List<Double> errors = clusters.stream()
				.map(c -> c.getSquaredCentroidError())
				.collect(Collectors.toList());

		assertEquals(ref.size(),errors.size());
		for(int i=0;i<ref.size();i++){
			assertEquals(ref.get(i),errors.get(i),0.01);
		}
	}
	
	@Test
	public void testGetClusters(){
		List<Cluster> clusters = clusterTree.getClusters(10);
		
		ClusterTestData.assertClustersEqual(refCompleteLinkageSet1,clusters);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetClustersZeroLimit(){
		clusterTree.getClusters(0);			
	}
	
	@Test
	public void testGetClustersOneLimit(){
		List<Cluster> clusters = clusterTree.getClusters(1);
		assertEquals(1,clusters.size());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetClustersOverLimit(){
		clusterTree.getClusters(500);
	}
	
	@Test
	public void testGetClustersAtLimit(){
		List<Cluster> clusters = clusterTree.getClusters(30);
		
		assertEquals(30, clusters.size());
	}
	
	@Test
	public void testGetClosestDistances(){
		List<Double> ref = Arrays.asList(
				0.00,0.10,0.10,
				0.14,0.14,0.14,
				0.14,0.14,0.20,
				0.20,0.22,0.30,
				0.30,0.32,0.42,
				0.50,0.54,0.54,
				0.54,0.61,0.64,
				0.72,0.89,0.95,
				1.33,1.41,1.60,
				3.20,7.60);
		
		List<Double> distances = clusterTree.getClosestDistances();
			
		assertEquals(ref.size(),distances.size());
		for(int i=0;i<ref.size();i++){
			assertEquals(ref.get(i),distances.get(i),0.01);
		}
				
	}

}
