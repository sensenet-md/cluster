package com.tcb.cluster;

import java.util.Map;

import com.tcb.matrix.LabeledMatrix;
import com.tcb.matrix.Matrix;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.tree.node.Node;
import com.tcb.tree.tree.Tree;

public interface ClusterDistance {
	public double getDistance(
			Node a, Node b, Tree tree,
			LabeledMatrix<Node> nodeDistances);
}
