package com.tcb.cluster.log;

public interface ParameterReporter {
	public String reportParameters();
}
