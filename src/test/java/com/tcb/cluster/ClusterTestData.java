package com.tcb.cluster;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.common.util.SafeMap;
import com.tcb.matrix.TriangularMatrix;

public class ClusterTestData {
		
	private List<String> columns = Arrays.asList(
		"sepalLengths",
		"sepalWidths",
		"petalLengths",
		"petalWidths");
	
	public Map<String,List<Double>> readShortData() throws IOException{
		return readData("resources/test/iris.data.short");
	}
	
	public Map<String,List<Double>> readData() throws IOException{
		return readData("resources/test/iris.data");
	}
	
	public static TriangularMatrix getDistances(List<Double> X, List<Double> Y){
		TriangularMatrix m = new TriangularMatrix(X.size());
		for(int i=0;i<X.size();i++){
			for(int j=i+1;j<X.size();j++){
				Double x1 = X.get(i);
				Double y1 = Y.get(i);
				Double x2 = X.get(j);
				Double y2 = Y.get(j);
				m.set(i, j, getDistance(x1,y1,x2,y2));
			}
		}
		return m;
	}
	
	private static Double getDistance(Double x1, Double y1, Double x2, Double y2){
		return Math.sqrt(
				Math.pow(x2-x1, 2)
				+ Math.pow(y2-y1, 2));
	}
	
	public static void assertClustersEqual(List<List<Integer>> ref, List<Cluster> clusters){
		List<List<String>> testIndices = clusters.stream()
				.map(c -> c.getData())
				.collect(Collectors.toList());

		for(int i=0;i<ref.size();i++){
			List<Integer> refCluster = ref.get(i);
			List<Integer> testCluster = testIndices.get(i).stream()
					.map(s -> Integer.valueOf(s))
					.collect(Collectors.toList());
			for(int j=0;j<refCluster.size();j++){
				assertEquals(refCluster.get(j), testCluster.get(j));
			}
		}
	}
	
	private Map<String,List<Double>> readData(String path) throws IOException {
		Map<String,List<Double>> data = new SafeMap<String,List<Double>>();
		CSV csv = new CSV_Reader(path,",",false).getCSV();
		for(int i=0;i<columns.size();i++){
			String column = columns.get(i);
			data.put(column, getDoubleColumn(csv,i));
		}
		return data;
	}
	
	private List<Double> getDoubleColumn(CSV csv, int idx){
		return csv.getColumns().get(idx)
		.stream()
		.map(s -> Double.parseDouble(s))
		.collect(Collectors.toList());
	}
	
}
