package com.tcb.cluster;

import java.util.List;

import com.tcb.common.util.Tuple;
import com.tcb.tree.node.Node;
import com.tcb.tree.tree.Tree;

public class ClusterStep {
		public Integer clusterCount;
		public Tuple<Node,Node> closestPair;
		public Double closestDistance;
		
		public ClusterStep(
				Integer clusterCount,
				Tuple<Node,Node> closestPair,
				Double closestDistance){
			this.clusterCount = clusterCount;
			this.closestPair = closestPair;
			this.closestDistance = closestDistance;
		}
}
