package com.tcb.cluster.limit;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.tcb.cluster.Cluster;
import com.tcb.cluster.ClusterImpl;
import com.tcb.cluster.ClusterTree;
import com.tcb.cluster.ClusterStep;
import com.tcb.cluster.log.ParameterReporter;
import com.tcb.matrix.TriangularMatrix;

public interface ClusterLimit extends ParameterReporter {
	public Boolean finished(ClusterStep clusterStep);
	public List<Cluster> getClusters(ClusterTree clusterTree);
}
