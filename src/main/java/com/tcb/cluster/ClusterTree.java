package com.tcb.cluster;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.tcb.common.util.ArrayUtil;
import com.tcb.matrix.LabeledMatrix;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.tree.node.Node;
import com.tcb.tree.tree.LeafNodeTreeSearcher;
import com.tcb.tree.tree.Tree;

public class ClusterTree {
	private Tree tree;
	private LabeledMatrix<String> distances;
	private LabeledMatrix<Node> nodeDistances;
	private List<Double> closestDistances;

	public ClusterTree(
			Tree tree,
			LabeledMatrix<String> distances,
			LabeledMatrix<Node> nodeDistances,
			List<Double> closestDistances){
		this.tree = tree;
		this.distances = distances;
		this.nodeDistances = nodeDistances;
		this.closestDistances = closestDistances;
	}
	
	public List<Double> getClosestDistances(){
		return closestDistances;
	}
	
	public Integer getDataPointCount(){
		return distances.getRowCount();
	}
	
	public List<Cluster> getClusters(Integer targetClusterCount){
		if(targetClusterCount < 1) throw new IllegalArgumentException("Cannot have less than 1 cluster");
		if(targetClusterCount > getDataPointCount()) throw new IllegalArgumentException("Cannot have more clusters than data points");
		List<Node> nodes = new ArrayList<>();
		nodes.add(tree.getRoot());
		while(nodes.size() < targetClusterCount){
			expandLevel(nodes);
		}
		return toClusters(nodes);
	}
		
	private void expandLevel(List<Node> nodes){
		int idx = getMaxSuidNodeIndex(nodes);
		Node n = nodes.remove(idx);
		nodes.addAll(tree.getChildren(n));
	}
	
	private List<Cluster> toClusters(List<Node> nodes){
		List<Cluster> result = new ArrayList<>();
		for(Node n:nodes){
			List<Node> leafNodes = LeafNodeTreeSearcher.getLeafNodes(n, tree);
			List<String> leafLabels = getIndices(leafNodes).stream()
					.sorted()
					.map(i -> distances.getLabel(i))
					.collect(ImmutableList.toImmutableList());
			String centroid = getCentroid(leafNodes);
			Double centroidSumOfSquaredErrors = getCentroidSquaredError(leafNodes);
			Cluster cluster = new ClusterImpl(leafLabels,centroid,centroidSumOfSquaredErrors);
			result.add(cluster);
		}
		result.sort(new ClusterComparator());
		return result;
	}
	
	private int getMaxSuidNodeIndex(List<Node> nodes){
		int idx = -1;
		Long suid = Long.MIN_VALUE;
		for(int i=0;i<nodes.size();i++){
			Node n = nodes.get(i);
			Long nSuid = n.getSuid();
			if(nSuid > suid){
				suid = nSuid;
				idx = i;
			}
		}
		return idx;
	}

	private String getCentroid(List<Node> data){
		Node centroid = getCentroidNode(data);
		Integer idx = nodeDistances.getIndex(centroid);
		return distances.getLabel(idx);
	}
		
	private Node getCentroidNode(List<Node> data){
		int dim = data.size();
		double[] sums = new double[dim];
		for(int i=0;i<dim;i++){
			for(int j=i+1;j<dim;j++){
				Node a = data.get(i);
				Node b = data.get(j);
				Double d = nodeDistances.get(a,b);
				sums[i] += d;
				sums[j] += d;
			}
		}
		int minIdx = ArrayUtil.indexOf(
				sums, ArrayUtil.getMin(sums));
		Node minNode = data.get(minIdx);
		return minNode;
	}
	
	private List<Integer> getIndices(List<Node> data){
		return data.stream()
				.map(n -> nodeDistances.getIndex(n))
				.collect(ImmutableList.toImmutableList());
	}
	
	private Double getCentroidSquaredError(List<Node> data){
		Double sse = 0d;
		Node centroid = getCentroidNode(data);
		for(Node n:data){
			if(n.equals(centroid)) continue;
			Double dist = nodeDistances.get(n, centroid);
			sse += Math.pow(dist,2);
		}
		return sse;
	}
}
