package com.tcb.cluster.agglomerative;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.ImmutableList;
import com.tcb.cluster.Cluster;
import com.tcb.cluster.ClusterComparator;
import com.tcb.cluster.ClusterFactory;
import com.tcb.cluster.ClusterImpl;
import com.tcb.cluster.ClusterStep;
import com.tcb.cluster.Clusterer;
import com.tcb.cluster.TreeClusterer;
import com.tcb.cluster.ClusterTree;
import com.tcb.cluster.limit.ClusterLimit;
import com.tcb.cluster.linkage.Linkage;
import com.tcb.matrix.LabeledMatrix;
import com.tcb.matrix.LabeledSquareMatrixImpl;
import com.tcb.matrix.Matrix;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.tree.node.Node;
import com.tcb.tree.tree.Tree;
import com.tcb.common.util.ArrayUtil;
import com.tcb.common.util.Combinatorics;
import com.tcb.common.util.Tuple;

public class AgglomerativeClusterer implements Clusterer,TreeClusterer {
		
	private Linkage linkage;
	private ConcurrentHashMap<Node,Map<Node,Double>> linkageDistanceCache;

	private volatile int currentClusterCount;
	
	protected volatile boolean cancelled = false;
	private LabeledMatrix<String> distances;
	
		
	public AgglomerativeClusterer(
			LabeledMatrix<String> distances,
			Linkage linkage
			){
		this.distances = distances;
		this.linkage = linkage;
		this.currentClusterCount = distances.getRowCount();
	}
	
	private ConcurrentHashMap<Node,Map<Node,Double>> createLinkageDistanceCache(){
		return new ConcurrentHashMap<Node,Map<Node,Double>>();		
	}
	
	@Override
	public List<Cluster> cluster(ClusterLimit limit){
		ClusterTree result = cluster();
		return limit.getClusters(result);
	}
	
	@Override
	public ClusterTree cluster(){
		linkageDistanceCache = createLinkageDistanceCache();
		
		Tree tree = new ClusterFactory(distances.getRowCount())
				.createStartClusters();
		
		currentClusterCount = getClusters(tree).size();
		LabeledMatrix<Node> nodeDistances = createNodeDistances(tree);
		
		Optional<ClusterStep> step = planNextStep(tree, nodeDistances);
		ImmutableList.Builder<Double> closestDistances = ImmutableList.builder();

		while(step.isPresent()){
			if(cancelled) throw new RuntimeException("Cancelled clustering");
			closestDistances.add(step.get().closestDistance);
			runStep(tree,step.get());
			step = planNextStep(tree,nodeDistances);
		}
		
		return new ClusterTree(tree, distances, nodeDistances, closestDistances.build());
	}
		
	private LabeledMatrix<Node> createNodeDistances(Tree tree){
		List<Node> clusters = getClusters(tree);
		LabeledMatrix<Node> result = LabeledSquareMatrixImpl.create(clusters, distances);
		return result;
	}
		
	private List<Node> getClusters(Tree tree){
		return tree.getChildren(tree.getRoot());
	}
	
	private void runStep(Tree tree, ClusterStep step){
		Tuple<Node,Node> closestPair = step.closestPair;
		tree.mergeNodes(closestPair.one(), closestPair.two());
		currentClusterCount--;
	}
	
	@Override
	public Integer getCurrentClusterCount(){
		return currentClusterCount;
	}
		
	private Optional<ClusterStep> planNextStep(Tree tree, LabeledMatrix<Node> nodeDistances){
		List<Node> clusters = getClusters(tree);
		Integer clusterCount = clusters.size();
		if(clusterCount <= 1) return Optional.empty();
		
		List<Tuple<Node,Node>> clusterPairs = Combinatorics.getCombinationsNoSelf(
				clusters);
		
		double[] pairDistances = clusterPairs.parallelStream()
				.map(p -> getCachedLinkageDistance(p.one(), p.two(), tree, nodeDistances))
				.mapToDouble(d -> d)
				.toArray();
		
		
		double minDistance = ArrayUtil.getMin(pairDistances);
		Tuple<Node,Node> closestPair = clusterPairs.get(
				ArrayUtil.indexOf(pairDistances, minDistance)
				);
				
		return Optional.of(
				new ClusterStep(
				clusterCount,
				closestPair,
				minDistance));
	}
		
	private double getLinkageDistance(Node cluster1, Node cluster2, Tree tree, LabeledMatrix<Node> nodeDistances){
		return linkage.getDistance(cluster1,cluster2, tree, nodeDistances);
	}
		
	private double getCachedLinkageDistance(Node cluster1, Node cluster2, Tree tree, LabeledMatrix<Node> nodeDistances){
		/* ATTENTION: This function is called in parallel, so special care is needed to make
		 * this Thread-safe. It is particularily important that the distance/pairDistance
		 * function always returns the same value for the same cluster arguments. For extra
		 * efficiency, it should be guaranteed that the arguments cluster1, cluster2
		 * are always entered in the same order.
		*/
		
		/* The cache should be a concurrent map, as those are guaranteed 
		 * to see the last committed state. This is important for the loop,
		 * as it prevents seeing a potential intermediate state where
		 * the map would have a key but the submap is not fully initialized.
		 * Note that it is not a problem if the submap is overridden due to parallelism;
		 * at most, we will lose a single result from the cache 
		 * which will have to be recalculated.
		*/
		if(!linkageDistanceCache.containsKey(cluster1)){
			linkageDistanceCache.put(cluster1, new ConcurrentHashMap<Node,Double>());
		}
		
		Map<Node,Double> subMap = linkageDistanceCache.get(cluster1);
		
		if(subMap.containsKey(cluster2)){
			return subMap.get(cluster2);
		} else {
			double d = getLinkageDistance(cluster1,cluster2, tree, nodeDistances);
			subMap.put(cluster2, d);
			return d;
		}
	}

	@Override
	public void cancel() {
		cancelled = true;		
	}
	
	
	
}
