package com.tcb.cluster;

import java.util.List;

public interface Cluster {
	public List<String> getData();
	public String getCentroid();
	public Double getSquaredCentroidError();

}