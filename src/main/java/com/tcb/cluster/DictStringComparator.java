package com.tcb.cluster;

import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;

public class DictStringComparator implements Comparator<String> {

	@Override
	public int compare(String a, String b) {
		if(StringUtils.isNumeric(a) && StringUtils.isNumeric(b)){
			return Integer.valueOf(a).compareTo(Integer.valueOf(b));
		} else {
			return a.compareTo(b);
		}
	}

}
