package com.tcb.cluster;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;

import com.tcb.cluster.Cluster;
import com.tcb.cluster.agglomerative.AgglomerativeClusterer;
import com.tcb.cluster.limit.ClusterLimit;
import com.tcb.cluster.limit.EpsilonLimit;
import com.tcb.cluster.limit.TargetNumberLimit;
import com.tcb.cluster.linkage.AverageLinkage;
import com.tcb.cluster.linkage.CompleteLinkage;
import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.matrix.LabeledMatrix;
import com.tcb.matrix.LabeledSquareMatrixImpl;
import com.tcb.matrix.Matrix;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.tree.node.Node;
import com.tcb.tree.tree.LeafNodeTreeSearcher;
import com.tcb.tree.tree.Tree;
import com.tcb.monitor.Monitor;
import com.tcb.monitor.MonitorImpl;
import com.tcb.common.util.SafeMap;

public class AgglomerativeClustererTest {

	private Map<String,List<Double>> irisData;
	
	private List<List<Integer>> refCompleteLinkageSet1 =
			Arrays.asList(
					Arrays.asList(11, 14, 16, 18, 20, 23, 24),
					Arrays.asList(1, 2, 3, 6, 8, 9),
					Arrays.asList(0, 4, 7),
					Arrays.asList(12, 22, 29),
					Arrays.asList(13, 15, 21),
					Arrays.asList(17, 19, 26),
					Arrays.asList(25, 27),
					Arrays.asList(5),
					Arrays.asList(10),
					Arrays.asList(28)
					);
	
	private List<List<Integer>> refAverageLinkageSet1 =
			Arrays.asList(
					Arrays.asList(11, 14, 16, 18, 20, 23, 24),
					Arrays.asList(1, 2, 3, 6, 8, 9),
					Arrays.asList(12, 22, 25, 27),
					Arrays.asList(0, 4, 7),
					Arrays.asList(13, 15, 21),
					Arrays.asList(17, 19, 26),
					Arrays.asList(5),
					Arrays.asList(10),
					Arrays.asList(28),
					Arrays.asList(29)
					);
	
	
	private List<List<Integer>> refCompleteLinkageSet2 =
			Arrays.asList(
					Arrays.asList(0, 1, 2, 3, 4, 6, 7, 8, 9),
					Arrays.asList(11, 14, 15, 18, 26),
					Arrays.asList(20, 22, 24, 29),
					Arrays.asList(10, 12, 16),
					Arrays.asList(13, 19),
					Arrays.asList(23, 28),
					Arrays.asList(25, 27),
					Arrays.asList(5),
					Arrays.asList(17),
					Arrays.asList(21)
					);
	
	private List<List<Integer>> refAverageLinkageSet2 =
			Arrays.asList(
					Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
					Arrays.asList(10, 11, 12, 14, 15, 16, 18, 26),
					Arrays.asList(13, 19),
					Arrays.asList(20, 29),
					Arrays.asList(22, 24),
					Arrays.asList(23, 28),
					Arrays.asList(17),
					Arrays.asList(21),
					Arrays.asList(25),
					Arrays.asList(27)
					);

	private TargetNumberLimit limit;
		
	@Before
	public void setUp() throws Exception {
		this.irisData = new ClusterTestData().readShortData();
		this.limit = new TargetNumberLimit(10);
	}
		
	protected List<String> createLabels(Matrix matrix){
		 return IntStream.range(0,matrix.getRowCount())
			.boxed()
			.map(i -> i.toString())
			.collect(Collectors.toList());
	}
		 
	

	@Test
	public void testClusterCompleteLinkageSet1() {
		TriangularMatrix distances = ClusterTestData.getDistances(irisData.get("sepalLengths"), irisData.get("sepalWidths"));
		List<String> labels = createLabels(distances);
		LabeledMatrix<String> matrix = LabeledSquareMatrixImpl.create(labels, distances);
		AgglomerativeClusterer clusterer = new AgglomerativeClusterer(matrix, new CompleteLinkage());
		List<Cluster> clusters = clusterer.cluster(limit);
		ClusterTestData.assertClustersEqual(refCompleteLinkageSet1, clusters);
	}
	
	@Test
	public void testClusterCompleteLinkageSet1EpsilonLimit() {
		TriangularMatrix distances = ClusterTestData.getDistances(irisData.get("sepalLengths"), irisData.get("sepalWidths"));
		List<String> labels = createLabels(distances);
		LabeledMatrix<String> matrix = LabeledSquareMatrixImpl.create(labels, distances);
		AgglomerativeClusterer clusterer = new AgglomerativeClusterer(matrix, new CompleteLinkage());
		ClusterLimit limit = new EpsilonLimit(0.21);
		ClusterTree clusters = clusterer.cluster();
				
		assertEquals(20, limit.getClusters(clusters).size());
		
		limit = new EpsilonLimit(0.53);
		
		clusters = clusterer.cluster();
		
		assertEquals(14, limit.getClusters(clusters).size());
	}
	
	@Test
	public void testClusterAverageLinkageSet1() {
		TriangularMatrix distances = ClusterTestData.getDistances(irisData.get("sepalLengths"), irisData.get("sepalWidths"));
		List<String> labels = createLabels(distances);
		LabeledMatrix<String> matrix = LabeledSquareMatrixImpl.create(labels, distances);
		AgglomerativeClusterer clusterer = new AgglomerativeClusterer(matrix, new AverageLinkage());
		List<Cluster> clusters = clusterer.cluster(limit);
		ClusterTestData.assertClustersEqual(refAverageLinkageSet1, clusters);
	}
	
	@Test
	public void testClusterCompleteLinkageSet2() {
		TriangularMatrix distances = ClusterTestData.getDistances(irisData.get("petalLengths"), irisData.get("petalWidths"));
		List<String> labels = createLabels(distances);
		LabeledMatrix<String> matrix = LabeledSquareMatrixImpl.create(labels, distances);
		AgglomerativeClusterer clusterer = new AgglomerativeClusterer(matrix, new CompleteLinkage());
		List<Cluster> clusters = clusterer.cluster(limit);
		ClusterTestData.assertClustersEqual(refCompleteLinkageSet2, clusters);
	}
	
	@Test
	public void testClusterAverageLinkageSet2() {
		TriangularMatrix distances = ClusterTestData.getDistances(irisData.get("petalLengths"), irisData.get("petalWidths"));
		List<String> labels = createLabels(distances);
		LabeledMatrix<String> matrix = LabeledSquareMatrixImpl.create(labels, distances);
		AgglomerativeClusterer clusterer = new AgglomerativeClusterer(matrix, new AverageLinkage());
		List<Cluster> clusters = clusterer.cluster(limit);
		ClusterTestData.assertClustersEqual(refAverageLinkageSet2, clusters);
	}
	
	@Test
	public void testClusterTiming() throws IOException {
		Map<String,List<Double>> irisDataComplete = new ClusterTestData().readData();
		TriangularMatrix distances = ClusterTestData.getDistances(irisDataComplete.get("sepalLengths"), irisDataComplete.get("sepalWidths"));
		List<String> labels = createLabels(distances);
		LabeledMatrix<String> matrix = LabeledSquareMatrixImpl.create(labels, distances);
		AgglomerativeClusterer clusterer = new AgglomerativeClusterer(matrix, new CompleteLinkage());
		
		Monitor monitor = new MonitorImpl("test");
		monitor.start();
		clusterer.cluster();
		monitor.stop();
		
		System.out.println(monitor.getAccumulatedTime());
	}
		
}
