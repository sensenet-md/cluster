package com.tcb.cluster.limit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import com.google.common.collect.ImmutableList;
import com.tcb.cluster.Cluster;
import com.tcb.cluster.ClusterDistance;
import com.tcb.cluster.ClusterImpl;
import com.tcb.cluster.ClusterTree;
import com.tcb.cluster.ClusterStep;
import com.tcb.cluster.log.LogBuilder;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.common.util.Combinatorics;
import com.tcb.common.util.Tuple;

public class EpsilonLimit implements ClusterLimit {

	private final Double distanceLimit;

	public EpsilonLimit(Double distanceLimit){
		this.distanceLimit = distanceLimit;
	}
	
	@Override
	public Boolean finished(ClusterStep clusterStep) {
		if(isOverLimit(clusterStep.closestDistance)){
			return true;
		} else {
			return false;
		}
	}
	
	private boolean isOverLimit(double d){
		return d > distanceLimit;
	}

	@Override
	public String reportParameters() {
		LogBuilder log = new LogBuilder();
		log.write(String.format("Limit: %s (%.2f)", "Epsilon", distanceLimit));
		return log.get();
	}

	@Override
	public List<Cluster> getClusters(ClusterTree result) {
		ImmutableList.Builder<Cluster> clusters = ImmutableList.builder();
		List<Double> closestDistances = result.getClosestDistances();
		int cnt = 1;
		for(int i=closestDistances.size()-1;i>=0;i--){
			Double dist = closestDistances.get(i);
			if(isOverLimit(dist)){
				cnt++;
			} else {
				break;
			}
		}
		clusters.addAll(result.getClusters(cnt));
		return clusters.build();
	}
	
	
	

	
}
