package com.tcb.cluster;

import java.util.List;

import com.tcb.cluster.limit.ClusterLimit;
import com.tcb.tree.tree.Tree;

public interface TreeClusterer extends Clusterer {
	public ClusterTree cluster();
}
