package com.tcb.cluster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tcb.matrix.TriangularMatrix;
import com.tcb.common.util.ArrayUtil;

public class ClusterImpl implements Cluster,Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<String> data;
	private String centroid;
	private Double squaredCentroidError;

	public ClusterImpl(List<String> data, String centroid, Double squaredCentroidError){
		this.data = data;
		this.centroid = centroid;
		this.squaredCentroidError = squaredCentroidError;
	}
	
	@Override
	public List<String> getData(){
		return data;
	}
		
	public String toString(){
		return data.toString();
	}

	@Override
	public String getCentroid() {
		return centroid;
	}

	@Override
	public Double getSquaredCentroidError() {
		return squaredCentroidError;
	}
	
	
}
