package com.tcb.cluster;

import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tcb.tree.node.Node;
import com.tcb.tree.tree.LeafNodeTreeSearcher;
import com.tcb.tree.tree.Tree;

public class ClusterComparator implements Comparator<Cluster> {
		
	@Override
	public int compare(Cluster a, Cluster b) {
		List<String> data1 = a.getData();
		List<String> data2 = b.getData();
		Integer size1 = data1.size();
		Integer size2 = data2.size();
		int c = size2.compareTo(size1);
		if(c==0){
			String first1 = data1.get(0);
			String first2 = data2.get(0);
			return new DictStringComparator().compare(first1, first2);
		}
		return c;
	}
}
