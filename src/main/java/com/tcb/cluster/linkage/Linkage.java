package com.tcb.cluster.linkage;

import com.tcb.cluster.ClusterDistance;
import com.tcb.cluster.ClusterImpl;
import com.tcb.cluster.log.ParameterReporter;
import com.tcb.matrix.TriangularMatrix;

public interface Linkage extends ClusterDistance, ParameterReporter {

}
