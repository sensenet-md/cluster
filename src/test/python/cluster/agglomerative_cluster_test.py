#!/usr/bin/env python3

from sklearn.cluster import AgglomerativeClustering
import pandas as pd
import numpy as np
import sys
from collections import defaultdict
import time

from scipy.cluster.hierarchy import linkage as link

converters = {
    0:float,
    1:float,
    2:float,
    3:float,
    4:str
}

columns = ['sepal-length','sepal-width',
           'petal-length', 'petal-width',
           'class']

iris = pd.read_csv('iris.data.short', converters=converters, header=None, names=columns)
n_clusters = 10

set1 = list(zip(iris['sepal-length'],iris['sepal-width']))
set2 = list(zip(iris['petal-length'],iris['petal-width']))

def run(dataset,linkage):
    model = AgglomerativeClustering(linkage=linkage,n_clusters=n_clusters)

    X = dataset

    Z = link(X, method=linkage)
    print(Z)

    predict = model.fit_predict(X)

    clusters = defaultdict(list)
    for i,x in enumerate(predict):
        clusters[x].append(i)

    clusterLst = [v for (k,v) in clusters.items()]
    clusterLst.sort(key=len,reverse=True)

    return clusterLst


def measure(f,*args):
    start = time.time()
    result = f(*args)
    end = time.time()
    t = end - start
    print("Elapsed: {0:.8f}".format(t))
    return result

print('complete linkage set1')
print(measure(run,set1,'complete'))

print('average linkage set1')
print(measure(run,set1,'average'))

print('complete linkage set2')
print(measure(run,set2,'complete'))

print('average linkage set2')
print(measure(run,set2,'average'))
