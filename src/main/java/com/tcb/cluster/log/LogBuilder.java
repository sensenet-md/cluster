package com.tcb.cluster.log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LogBuilder implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<String> data;

	public LogBuilder(){
		clear();
	}
	
	public void clear(){
		data = new ArrayList<>();
	}
	
	public void write(String s){
		data.add(s);
	}
		
	public String get(){
		String separator = System.getProperty("line.separator");
		return data.stream()
				.collect(Collectors.joining(separator));
	}
	
	
}
