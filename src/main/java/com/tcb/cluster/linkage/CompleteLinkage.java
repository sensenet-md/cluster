package com.tcb.cluster.linkage;

import java.util.Map;

import com.tcb.cluster.Cluster;
import com.tcb.cluster.Clusters;
import com.tcb.cluster.log.LogBuilder;
import com.tcb.matrix.LabeledMatrix;
import com.tcb.matrix.Matrix;
import com.tcb.matrix.TriangularMatrix;
import com.tcb.tree.node.Node;
import com.tcb.tree.tree.Tree;
import com.tcb.common.util.ArrayUtil;

public class CompleteLinkage implements Linkage {
	
	@Override
	public double getDistance(Node a, Node b, Tree tree, LabeledMatrix<Node> distances) {
		double[] pairDistances = Clusters.getPairDistances(a,b, tree, distances);
		return ArrayUtil.getMax(pairDistances);
	}

	@Override
	public String reportParameters() {
		LogBuilder log = new LogBuilder();
		log.write(String.format("Linkage: %s", LinkageStrategy.COMPLETE.toString()));
		return log.get();
	}
	
	
	
}
