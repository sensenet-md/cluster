package com.tcb.cluster;

import java.util.List;

import com.tcb.cluster.limit.ClusterLimit;
import com.tcb.tree.tree.Tree;

public interface Clusterer {
	public List<Cluster> cluster(ClusterLimit limit);
	public Integer getCurrentClusterCount();
	public void cancel();
}
